import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
 
  title: string = "My first angular app";
  helpText: string = "Help text";
  isAdmin: boolean = true;
  currentColor: Color = Color.Green;
  products: Array<string> = [ "banana", "apple", "orange" ];
  size: number = 16;

  classes: any = {
    "red-text": this.isAdmin,
    "underline-text": this.isAdmin,
    "consolas": !this.isAdmin
  }

  styles: any = {
    "background-color": "red",
    "color": "green"
  }

  onButtonClick(): void {
    this.title = "Clicked!";
    this.isAdmin = !this.isAdmin;
    this.size++;

    this.classes = {
      "red-text": this.isAdmin,
      "underline-text": this.isAdmin,
      "consolas": !this.isAdmin
    }

    this.styles = {
      "background-color": "green",
      "color": "red"
    }
  }

  onNumberClick(num: number) {
    this.title = num.toString(); 
  }

  Color = Color;

}

enum Color { Red, Green, Blue };
import { Component } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss']
})
export class CalcComponent {

  num1: number = 0;
  num2: number = 0;
  operation: string = '*';
  operations: Array<string> = ['+', '-', '*', '/'];

  get result(): number {
    switch (this.operation) {
      case '+':
        return this.num1 + this.num2;
      case '-':
        return this.num1 - this.num2;
      case '*':
        return this.num1 * this.num2;
      case '/':
        return this.num1 / this.num2;
      default:
        return 0;
    }
  } 




  // onCalc() {
  //   switch (this.operation) {
  //     case '+':
  //       this.result = this.num1 + this.num2;
  //       break;
  //     case '-':
  //       this.result = this.num1 - this.num2;
  //       break;
  //     case '*':
  //       this.result = this.num1 * this.num2;
  //       break;
  //     case '/':
  //       this.result = this.num1 / this.num2;
  //       break;
  //   }
  // }





  //text: string = "Empty";

  // onTextClick(): void {
  //   this.text = 'CLICKED!'; 
  // }

  // onKeyUp(event: KeyboardEvent): void {
  //   this.text = event.target.value;
  // }

  // onKeyUp(text: string): void {
  //   this.text = text;
  // }
}

import { Component } from "@angular/core";

@Component({
    selector: 'app-test',
    template: `
        <p>{{title}}</p>
    `,
    styles: [`
        p {
            color: red;
        }
    `]
})
export class TestComponent {
    title: string = "My text!";
}
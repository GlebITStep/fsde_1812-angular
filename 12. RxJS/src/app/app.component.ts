import { Component, OnInit } from '@angular/core';
import { Observable, Observer, interval, timer, fromEvent, of, Subject, BehaviorSubject, ReplaySubject, AsyncSubject, asapScheduler, asyncScheduler, queueScheduler } from 'rxjs';
import { map, filter, delay, distinct, distinctUntilChanged, debounce, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `
    <!-- <button id="btn">{{observable | async}}</button> -->
    <input type="text" id="txt">
  `,
  styles: []
})
export class AppComponent implements OnInit {
  observable: Observable<any> = null;
  observer: Observer<any> = null;
  subject: Subject<any> = null;
  //result: number = 0;

  async ngOnInit(): Promise<void> {
    // this.observable = new Observable<number>(subscriber => {
    //   subscriber.next(1);
    //   subscriber.next(2);
    //   // subscriber.error('Some error!');
    //   subscriber.next(3);
    //   subscriber.complete();
    // });


    // this.observer = {
    //   next: function(value: number) {
    //     console.log(value); 
    //   },
    //   error: function(error: any) {
    //     console.error(error)
    //   },
    //   complete: function() {
    //     console.log('DONE!'); 
    //   }
    // }


    // this.observable.subscribe(this.observer);


    // let data = await this.observable.toPromise();
    // console.log(data); 


    // this.observable.subscribe(
    //   next => console.log(next),
    //   error => console.error('Something happens!'),
    //   () => console.log('DONE'));


    // this.observable = new Observable<number>(subscriber => {
    //   setTimeout(() => subscriber.next(1), 2000);
    //   setTimeout(() => subscriber.next(2), 1000);
    //   // setTimeout(() => subscriber.error('TEST ERROR'), 2500);
    //   setTimeout(() => subscriber.next(3), 3000);
    //   setTimeout(() => subscriber.complete(), 5000);
    // });

    // this.observable.subscribe(x => console.log(x));


    //this.observable = interval(1000);
    //this.observable = timer(1000);
    // this.observable = 
    //   fromEvent<MouseEvent>(document.getElementById('btn'), 'mousemove')
    //   .pipe(
    //     map(event => { return { x: event.x, y: event.y } }),
    //     filter(event => event.x > 40),
    //     delay(1500)
    //   );

    // this.observable.subscribe(x => console.log(x));


    // this.observable = interval(1000);

    // //this.observable.subscribe(x => this.result = x);


    this.observable = fromEvent<KeyboardEvent>(document.getElementById('txt'), 'keyup')
    .pipe(
      debounceTime(1000),
      map(x => (x.target as HTMLInputElement).value),
      filter(x => x.length > 2),
      distinctUntilChanged()
    );

    this.observable.subscribe(x => console.log(x));
    
    //---------------------------------------------------------

    //timer
    //interval
    //ajax
    //fromEvent

    // this.observable = interval(1000);

    // this.observable.subscribe(x => console.log('First: ' + x));

    // setTimeout(() => {
    //   this.observable.subscribe(x => console.log('Second: ' + x));    
    // }, 2500);

    // timer(4000).subscribe(
    //   () => this.observable.subscribe(x => console.log('Third: ' + x)));


    // this.subject = new Subject<any>();
    this.subject = new BehaviorSubject<any>(0);
    // this.subject = new ReplaySubject(3);
    // this.subject = new AsyncSubject();
    
    this.subject.subscribe(x => console.log('First: ' + x));

    this.subject.next(1);
    this.subject.next(2);
    this.subject.next(3);
    
    this.subject.subscribe(x => console.log('Second: ' + x));
    
    timer(2000).subscribe(() => this.subject.next(4));
    timer(4000).subscribe(() => this.subject.next(5));

    // this.subject.complete();
  }
}

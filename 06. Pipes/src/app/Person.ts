export interface Person {
  id: number;
  fullName: string;
  date: Date;
  gender: string;
}

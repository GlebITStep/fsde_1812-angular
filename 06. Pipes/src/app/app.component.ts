import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from './Person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  people: Array<Person> = [];
  peopleCount: number = 10;

  constructor(private httpClient: HttpClient) {}

  async ngOnInit(): Promise<void> {
    try {
      this.people = await this.httpClient.get<Array<Person>>('assets/people.json').toPromise(); 
    } catch (error) {
      console.error(error);
    }
  }



}


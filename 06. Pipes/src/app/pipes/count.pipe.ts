import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'count'
})
export class CountPipe implements PipeTransform {

  transform(arr: Array<any>, num: number): Array<any> {
    return arr.slice(0, num);
  }

}

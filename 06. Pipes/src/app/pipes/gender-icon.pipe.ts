import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'genderIcon'
})
export class GenderIconPipe implements PipeTransform {

  transform(value: string): string {
    if (value.toLowerCase() == 'male') return '🚹';
    else if (value.toLowerCase() == 'female') return '🚺';
    else return '⁉';
  }

}

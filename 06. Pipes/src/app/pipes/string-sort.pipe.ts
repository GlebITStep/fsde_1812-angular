import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '../Person';

@Pipe({
  name: 'stringSort'
})
export class StringSortPipe implements PipeTransform {

  compareNames(first: Person, last: Person) : number {
    if (first.fullName > last.fullName) {
      return 1;
    } else if (first.fullName < last.fullName) {
      return -1;
    } else {
      return 0;
    }
  }

  transform(arr: Array<Person>): Array<Person> {
    return arr.sort(this.compareNames);
  }

}

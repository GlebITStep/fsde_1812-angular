import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bus } from 'src/app/models/bus';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  lat: number = 40.3984603;
  lng: number = 49.8725311;
  theme: any;
  buses: Array<Bus> = [];

  constructor(private httpClient: HttpClient) { }

  async ngOnInit() {
    await this.loadTheme();
    await this.getBusInfo();
    setInterval(async () => {
      this.getBusInfo();
    }, 10000);
  }

  async getBusInfo() {
    var result = await this.httpClient.get<Array<Bus>>('https://localhost:44368/api/businfo').toPromise();
    this.buses = result.filter(x => x.routeCode == '2');
    console.log(this.buses); 
  }

  async loadTheme() {
    this.theme = await this.httpClient.get<any>('assets/theme.json').toPromise();
  }
}

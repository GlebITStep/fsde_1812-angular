﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BakuBusApi.Controllers
{
    public class ApiController : Controller
    {
        private readonly IHttpClientFactory httpClientFactory;

        public ApiController(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public IActionResult Ping()
        {
            return Json("TEST");
        }

        public async Task<IActionResult> BusInfo()
        {
            var httpClient = httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync("https://www.bakubus.az/az/ajax/apiNew1");
            var json = await response.Content.ReadAsStringAsync();
            //var jsonSerializerOptions = new JsonSerializerOptions();
            //var data = JsonSerializer.Deserialize<BakuBusApiResponse>(json);
            var data = JsonConvert.DeserializeObject<BakuBusApiResponse>(json, new JsonSerializerSettings
            {
                Culture = new System.Globalization.CultureInfo("tr-TR")
            });

            var result = data.BUS.Select(x => x.attributes);
            return Json(result);
        }
    }


    public class BakuBusApiResponse
    {
        public Bus[] BUS { get; set; }
    }

    public class Bus
    {
        [JsonProperty("@attributes")]
        public BusInfo attributes { get; set; }
    }

    public class BusInfo
    {

        [JsonProperty("BUS_ID")]
        public string BusId { get; set; }

        [JsonProperty("PLATE")]
        public string Plate { get; set; }

        [JsonProperty("LATITUDE")]
        public double Latitude { get; set; }

        [JsonProperty("LONGITUDE")]
        public double Longitude { get; set; }

        [JsonProperty("DISPLAY_ROUTE_CODE")]
        public string RouteCode { get; set; }



        //public string DRIVER_NAME { get; set; }
        //public string CURRENT_STOP { get; set; }
        //public string PREV_STOP { get; set; }
        //public string SPEED { get; set; }
        //public string BUS_MODEL { get; set; }
        //public string BUS_MODEL { get; set; }
        //public string ROUTE_NAME { get; set; }
        //public string LAST_UPDATE_TIME { get; set; }
        //public string SVCOUNT { get; set; }
    }
}

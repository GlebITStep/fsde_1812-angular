import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!-- <app-map [selectedBusNumber]="selectedBus"></app-map>
    <app-bus-select (changed)="onBusChanged($event)"></app-bus-select> -->
    <app-map></app-map>
    <app-bus-select></app-bus-select>
  `,
  styles: []
})
export class AppComponent {

  // selectedBus: string;

  // onBusChanged(busNumber: string) {
  //   this.selectedBus = busNumber;
  // }

}

import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bus } from 'src/app/models/bus';
import { BusStorageService } from 'src/app/services/bus-storage.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  lat: number = 40.3984603;
  lng: number = 49.8725311;
  theme: any;
  buses: Array<Bus> = [];
  //@Input() selectedBusNumber: string = '0';

  constructor(
    private httpClient: HttpClient,
    public busStorage: BusStorageService) { }

  async ngOnInit() {
    await this.loadTheme();
    await this.getBusInfo();
    setInterval(async () => {
      this.getBusInfo();
    }, 10000);
  }

  async getBusInfo() {
    this.buses = await this.httpClient.get<Array<Bus>>('https://localhost:44368/api/businfo').toPromise();
  }

  async loadTheme() {
    this.theme = await this.httpClient.get<any>('assets/theme.json').toPromise();
  }
}


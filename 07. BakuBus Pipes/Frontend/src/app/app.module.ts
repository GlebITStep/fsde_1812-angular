import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';
import { BusNumberPipe } from './pipes/bus-number.pipe';
import { BusSelectComponent } from './components/bus-select/bus-select.component';
import { BusStorageService } from './services/bus-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    BusNumberPipe,
    BusSelectComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC9esWPcCVfIkNLp5YJOLWxfHdy_Rl2xQg'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

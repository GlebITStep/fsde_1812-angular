import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Contact } from '../models/contact';
import { ContactApiService } from './contact-api.service';
import { delay, tap, filter } from 'rxjs/operators';

@Injectable()
export class ContactStorageService {

  private contacts: BehaviorSubject<Array<Contact>>;
  
  constructor() {
    this.contacts = new BehaviorSubject(null);
  }

  get contacts$() {
    return this.contacts.asObservable();
  }

  set(contacts: Array<Contact>) {
    this.contacts.next(contacts);
  }

  create(contact: Contact) {
    this.contacts.next([...this.contacts.getValue(), contact]);
  }  

  remove(id: string) {
    this.contacts.next(this.contacts.getValue().filter(x => x.id != id));
  }






  // private contacts: BehaviorSubject<Array<Contact>>;
  // public contacts$: Observable<Array<Contact>>;
  
  // constructor(private contactApi: ContactApiService) {
  //   this.contacts = new BehaviorSubject(null);
  //   this.contacts$ = this.contacts.asObservable();
    
  //   this.contactApi.getAll()
  //     .pipe(delay(1000))
  //     .subscribe(data => this.contacts.next(data));
  // }

  // create(contact: Contact) : Observable<Contact> {
  //   return this.contactApi.create(contact).pipe(
  //     tap(data => this.contacts.next([...this.contacts.getValue(), data])));
  // }

  // remove(id: string) : Observable<void> {
  //   return this.contactApi.remove(id).pipe(
  //     tap(() => this.contacts.next(this.contacts.getValue().filter(x => x.id != id)))); 
  // }
}

import { Injectable } from '@angular/core';
import { ContactApiService } from './contact-api.service';
import { ContactStorageService } from './contact-storage.service';
import { Contact } from '../models/contact';
import { tap, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class ContactFacadeService {

  constructor(
    private contactApi: ContactApiService,
    private contactStorage: ContactStorageService) { 
      this.load();
  }

  get contacts$() {
    return this.contactStorage.contacts$;
  }

  get(id: string): Observable<Contact> {
    return this.contactApi.getById(id);
  }

  create(contact: Contact) : Observable<Contact> {
    return this.contactApi.create(contact).pipe(
      tap(data => this.contactStorage.create(data)));
  }

  remove(id: string) : Observable<void> {
    return this.contactApi.remove(id).pipe(
      tap(() => this.contactStorage.remove(id)));
  }

  searchByPhone(phone: string): Observable<Array<Contact>> {
    return this.contactApi.searchByPhone(phone);
  }

  private load() {
    this.contactApi.getAll()
      .pipe(delay(1000))
      .subscribe(data => this.contactStorage.set(data));
  }
}

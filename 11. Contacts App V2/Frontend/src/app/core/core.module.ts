import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactApiService } from './services/contact-api.service';
import { ContactStorageService } from './services/contact-storage.service';
import { ContactFacadeService } from './services/contact-facade.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ContactApiService,
    ContactStorageService,
    ContactFacadeService
  ]
})
export class CoreModule { }

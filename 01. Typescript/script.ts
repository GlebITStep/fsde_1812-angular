// let num: number = 5;
// let bool: boolean = true;
// let str: string = 'Hello!';



// let something: any;
// something = 123;
// console.log(something);
// something = "hello";
// console.log(something);



// //let arr: number[] = [ 1, 2, 3 ];
// let arr: Array<number> = [ 1, 2, 3 ];



// enum Color { Red, Green, Blue };
// let color: Color = Color.Blue;



// let tuple: [string, number] = ['one', 2];
// console.log(tuple[0]);
// console.log(tuple[1]);




// let age: number = 25;
// let myname: string = 'Gleb';
// Foo(123, 'qwe');

// function Foo(ageParam: number, nameParam: string): number
// {
//     console.log(ageParam + ' ' + nameParam); 
//     return 42;
// }




// enum Gender { Male, Female };

// interface Person
// {
//     age: number;
//     name: string;
//     gender?: Gender;
// }

// let p: Person = { name: 'Gleb', age: 25, gender: Gender.Male };
// PrintPerson({ name: 'Gleb', age: 25 });

// function PrintPerson(person: Person): void
// {
//     console.log(`Name: ${person.name}`);
//     console.log(`Age:  ${person.age}`);
// }





// enum Gender { Male, Female };

// // class Person
// // {
// //     age: number = 0;
// //     name: string = 'Unknown';
// //     gender?: Gender;

// //     constructor(age: number, name: string, gender: Gender = Gender.Male) {
// //         this.age = age;
// //         this.name = name;
// //         this.gender = gender;
// //     }

// //     showInfo(): void
// //     {
// //         console.log(`Name: ${this.name}`);
// //         console.log(`Age:  ${this.age}`);
// //     }
// // }

// class Person {
//     readonly id: number;
//     static count: 0;

//     constructor(
//         public age: number, 
//         public name: string, 
//         public gender: Gender = Gender.Male) {
//             this.id = ++Person.count;
//     }

//     // showInfo(): void
//     // {
//     //     console.log(`Name: ${this.name}`);
//     //     console.log(`Age:  ${this.age}`);
//     // }
    
//     private _salary: number = 0;
    
//     public get salary(): number {
//         return this._salary;
//     }

//     public set salary(value: number) {
//         if(value > 0)
//             this._salary = value;
//     }    
// }

// interface IPrintable {
//     print(): void
// }

// interface ITest2 {

// }

// class Student extends Person implements IPrintable, ITest2 {
    
//     constructor(age: number, name: string, gender: Gender = Gender.Male) {
//         super(age, name, gender);
//     }

//     print(): void {
//         console.log(`Name: ${this.name}`);
//         console.log(`Age:  ${this.age}`);
//     }

// }

// class MyList<T> {
//     private arr: T[] = [];
    
//     add(item: T) {
//         this.arr.push(item);
//     }
// }

// interface Func {
//     (text: string): void
// }

// function Print(text: string): void {
//     console.log(text); 
// }

// function PrintUpper(text: string): void {
//     console.log(text.toUpperCase()); 
// }

// let func: Func;
// func = Print;
// func('Hello');
// func = PrintUpper;
// func('Hello');

// // function Test(func: Func, text: string) {
// //     func(text);
// // }

// // Test(x => 'Hello', 'Test');

// let list: MyList<number>;
// let p: Person = new Person(25, 'Gleb');
// p.salary = 1234;
// console.log(p.salary);
// PrintPerson(p);

// function PrintPerson(person: Person): void
// {
//     console.log(`Name: ${person.name}`);
//     console.log(`Age:  ${person.age}`);
// }



import * as Lib from 'Test';
let p: Lib.Person;
let c: Lib.Car;
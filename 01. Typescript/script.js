// let num: number = 5;
// let bool: boolean = true;
// let str: string = 'Hello!';
// let something: any;
// something = 123;
// console.log(something);
// something = "hello";
// console.log(something);
// //let arr: number[] = [ 1, 2, 3 ];
// let arr: Array<number> = [ 1, 2, 3 ];
// enum Color { Red, Green, Blue };
// let color: Color = Color.Blue;
// let tuple: [string, number] = ['one', 2];
// console.log(tuple[0]);
// console.log(tuple[1]);
// let age: number = 25;
// let myname: string = 'Gleb';
// Foo(123, 'qwe');
// function Foo(ageParam: number, nameParam: string): number
// {
//     console.log(ageParam + ' ' + nameParam); 
//     return 42;
// }
// enum Gender { Male, Female };
// interface Person
// {
//     age: number;
//     name: string;
//     gender?: Gender;
// }
// let p: Person = { name: 'Gleb', age: 25, gender: Gender.Male };
// PrintPerson({ name: 'Gleb', age: 25 });
// function PrintPerson(person: Person): void
// {
//     console.log(`Name: ${person.name}`);
//     console.log(`Age:  ${person.age}`);
// }
var Gender;
(function (Gender) {
    Gender[Gender["Male"] = 0] = "Male";
    Gender[Gender["Female"] = 1] = "Female";
})(Gender || (Gender = {}));
;
var Person = /** @class */ (function () {
    function Person(age, name, gender) {
        if (gender === void 0) { gender = Gender.Male; }
        this.age = 0;
        this.name = 'Unknown';
    }
    return Person;
}());
var p = new Person(25, 'Gleb');
PrintPerson({ name: 'Gleb', age: 25 });
function PrintPerson(person) {
    console.log("Name: " + person.name);
    console.log("Age:  " + person.age);
}

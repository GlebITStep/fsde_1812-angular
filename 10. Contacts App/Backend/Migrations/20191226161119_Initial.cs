﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactsAppApi.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false, defaultValueSql: "NEWID()"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Surname = table.Column<string>(maxLength: 100, nullable: true),
                    Phone = table.Column<string>(maxLength: 100, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "Email", "Name", "Phone", "Surname" },
                values: new object[,]
                {
                    { "bc53db05-7a95-43bf-b30d-4087da5f9ab7", null, "Daryn", "945-877-2718", "Manssuer" },
                    { "f385d103-c099-4bfd-9ebf-b8f163a02ce7", "aliepins1@google.com", "Allsun", "963-476-5190", "Liepins" },
                    { "f4aa837a-8a72-4fec-89da-51bf00ab58c2", null, "Margarethe", "998-245-2843", "Tolmie" },
                    { "adb9245b-8344-425c-bd1f-dc8bbded48c8", "gcocher3@washington.edu", "Gilli", "104-263-3283", "Cocher" },
                    { "42b18ad4-ded6-4343-830e-c6201da39494", null, "Jarret", "287-172-2275", "Macconaghy" },
                    { "baac67e4-623c-4c20-93a5-d91b0c2f7faa", "csomerlie5@forbes.com", "Carmelle", "345-499-9348", "Somerlie" },
                    { "881e3ade-afb0-4151-85d5-ae544fa49efd", null, "Ashleigh", "271-608-1059", "Trinder" },
                    { "5ce09475-8313-4c95-a0ac-7fbb1caa049d", "tmcmeekin7@hatena.ne.jp", "Tiphanie", "318-352-2038", "McMeekin" },
                    { "a7cd8319-e15b-437b-81f4-b89a183f318d", null, "Leandra", "251-357-8413", "Willows" },
                    { "95300f94-6915-41d0-bf61-6ee9fd1821c8", null, "Arman", "181-884-8749", "Brehault" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");
        }
    }
}

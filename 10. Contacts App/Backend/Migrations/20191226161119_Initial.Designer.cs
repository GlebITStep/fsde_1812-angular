﻿// <auto-generated />
using ContactsAppApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ContactsAppApi.Migrations
{
    [DbContext(typeof(ContactsAppDbContext))]
    [Migration("20191226161119_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ContactsAppApi.Models.Contact", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("nvarchar(450)")
                        .HasDefaultValueSql("NEWID()");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Surname")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.ToTable("Contacts");

                    b.HasData(
                        new
                        {
                            Id = "bc53db05-7a95-43bf-b30d-4087da5f9ab7",
                            Name = "Daryn",
                            Phone = "945-877-2718",
                            Surname = "Manssuer"
                        },
                        new
                        {
                            Id = "f385d103-c099-4bfd-9ebf-b8f163a02ce7",
                            Email = "aliepins1@google.com",
                            Name = "Allsun",
                            Phone = "963-476-5190",
                            Surname = "Liepins"
                        },
                        new
                        {
                            Id = "f4aa837a-8a72-4fec-89da-51bf00ab58c2",
                            Name = "Margarethe",
                            Phone = "998-245-2843",
                            Surname = "Tolmie"
                        },
                        new
                        {
                            Id = "adb9245b-8344-425c-bd1f-dc8bbded48c8",
                            Email = "gcocher3@washington.edu",
                            Name = "Gilli",
                            Phone = "104-263-3283",
                            Surname = "Cocher"
                        },
                        new
                        {
                            Id = "42b18ad4-ded6-4343-830e-c6201da39494",
                            Name = "Jarret",
                            Phone = "287-172-2275",
                            Surname = "Macconaghy"
                        },
                        new
                        {
                            Id = "baac67e4-623c-4c20-93a5-d91b0c2f7faa",
                            Email = "csomerlie5@forbes.com",
                            Name = "Carmelle",
                            Phone = "345-499-9348",
                            Surname = "Somerlie"
                        },
                        new
                        {
                            Id = "881e3ade-afb0-4151-85d5-ae544fa49efd",
                            Name = "Ashleigh",
                            Phone = "271-608-1059",
                            Surname = "Trinder"
                        },
                        new
                        {
                            Id = "5ce09475-8313-4c95-a0ac-7fbb1caa049d",
                            Email = "tmcmeekin7@hatena.ne.jp",
                            Name = "Tiphanie",
                            Phone = "318-352-2038",
                            Surname = "McMeekin"
                        },
                        new
                        {
                            Id = "a7cd8319-e15b-437b-81f4-b89a183f318d",
                            Name = "Leandra",
                            Phone = "251-357-8413",
                            Surname = "Willows"
                        },
                        new
                        {
                            Id = "95300f94-6915-41d0-bf61-6ee9fd1821c8",
                            Name = "Arman",
                            Phone = "181-884-8749",
                            Surname = "Brehault"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}

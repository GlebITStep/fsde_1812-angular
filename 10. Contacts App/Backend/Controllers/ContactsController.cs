﻿using AutoMapper;
using ContactsAppApi.Models;
using ContactsAppApi.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContactsAppApi.Controllers.V1
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    [Produces("application/json")]
    public class ContactsController : ControllerBase
    {
        private readonly ContactsAppDbContext context;
        private readonly IMapper mapper;

        public ContactsController(
            ContactsAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        ///     Get all contacts
        /// </summary>
        /// <returns>All contacts</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<ContactDTO>> GetAll() 
        {
            return Ok(mapper.Map<IEnumerable<ContactDTO>>(context.Contacts));
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<ContactDTO> GetById(string id)
        {
            if (id == null)
                return BadRequest();

            var contact = context.Contacts
                .Include(x => x.AdditionalPhones)
                .FirstOrDefault(x => x.Id == id);

            if (contact == null)
                return NotFound();

            var dto = mapper.Map<ContactDTO>(contact);

            return Ok(dto);
        }

        // POST /contacts
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public ActionResult<ContactDTO> Create(ContactDTO dto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var contact = mapper.Map<Contact>(dto);

            try
            {
                context.Contacts.Add(contact);
                context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            dto.Id = contact.Id;

            return Created($"/api/v1/contacts/{dto.Id}", dto);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(string id)
        {
            if (id == null)
                return BadRequest();

            var contact = context.Contacts.Find(id);

            if (contact == null)
                return NotFound();

            try
            {
                context.Contacts.Remove(contact);
                context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Update(string id, Contact contact)
        {
            if (id == null || contact == null || contact.Id != id)
                return BadRequest();

            try
            {
                context.Contacts.Update(contact);
                context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return Ok(contact);
        }

        [HttpPatch]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Patch(string id, JsonPatchDocument<Contact> contactPatch)
        {
            if (id == null || contactPatch == null)
                return BadRequest();

            var contact = context.Contacts.Find(id);

            if (contact == null)
                return NotFound();

            contactPatch.ApplyTo(contact);

            try
            {
                context.Contacts.Update(contact);
                context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return Ok(contact);
        }

        [HttpHead]
        [Route("head")]
        public ActionResult<string> Head()
        {
            return "head";
        }

        [HttpOptions]
        [Route("options")]
        public ActionResult<string> Options()
        {
            return "options";
        }
    }
}

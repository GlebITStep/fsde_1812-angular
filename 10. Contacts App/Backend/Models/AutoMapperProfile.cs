﻿using AutoMapper;
using ContactsAppApi.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactsAppApi.Models
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ContactDTO, Contact>()
                .ForMember(
                    x => x.AdditionalPhones,
                    x => x.MapFrom(y => y.AdditionalPhones.Select(z => new AdditionalPhone { Phone = z })));

            CreateMap<Contact, ContactDTO>()
                .ForMember(
                    x => x.AdditionalPhones, 
                    x => x.MapFrom(y => y.AdditionalPhones.Select(z => z.Phone)));
        }
    }
}

﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContactsAppApi.Models
{
    public class ContactsAppDbContext : DbContext
    {
        public ContactsAppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .Property(u => u.Id)
                .IsRequired()
                .HasDefaultValueSql("NEWID()");

            modelBuilder.Entity<Contact>()
                .Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Contact>()
                .Property(x => x.Surname)
                .HasMaxLength(100);

            modelBuilder.Entity<Contact>()
                .Property(x => x.Phone)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Contact>()
                .Property(x => x.Email)
                .HasMaxLength(100);

            var json = System.IO.File.ReadAllText("contacts.json");
            var options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            var contacts = JsonSerializer.Deserialize<List<Contact>>(json, options);

            modelBuilder.Entity<Contact>().HasData(contacts);
        }

        public override int SaveChanges()
        {
            AddUpdatedTime();
            return base.SaveChanges();
        }

        private void AddUpdatedTime()
        {
            var updatedEntities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity && x.State == EntityState.Modified);

            foreach (var item in updatedEntities)
            {
                (item.Entity as BaseEntity).UpdatedAt = DateTime.Now;
            }
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<AdditionalPhone> AdditionalPhones { get; set; }
    }
}

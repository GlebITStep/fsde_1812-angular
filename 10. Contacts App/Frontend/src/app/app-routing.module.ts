import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ErrorComponent } from './components/error/error.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { ContactFormReactiveComponent } from './components/contact-form-reactive/contact-form-reactive.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ContactListComponent },
  { path: 'add', component: ContactFormComponent },
  { path: 'add-reactive', component: ContactFormReactiveComponent },
  { path: 'contact/:id', component: ContactDetailsComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { environment } from '../../environments/environment';

import { Injectable } from '@angular/core';
import { Contact } from '../models/contact';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactApiService {

  readonly url: string = environment.contactsApiUrl;

  constructor(private httpClient: HttpClient) {}

  getAll(): Promise<Array<Contact>> {
    return this.httpClient
      .get<Array<Contact>>(this.url).toPromise();      
  }

  getById(id: string): Promise<Contact> {
    return this.httpClient
      .get<Contact>(`${this.url}/${id}`).toPromise();      
  }

  create(contact: Contact): Promise<Contact> {
    return this.httpClient
      .post<Contact>(this.url, contact).toPromise();      
  }

}

import { Contact } from '../models/contact';

export abstract class ContactService {
  
  abstract get contacts(): ReadonlyArray<Contact>;

  abstract get(id: string): Contact;
  
  //add
  //remove
  //update

}

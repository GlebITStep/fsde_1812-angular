import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';
import { ContactApiService } from 'src/app/services/contact-api.service';
import { Router } from '@angular/router';
import { Contact } from 'src/app/models/contact';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  contacts: Array<Contact> = [];

  constructor(
    public contactService: ContactApiService) { }

  async ngOnInit() {
    this.contacts = await this.contactService.getAll();
  }

}

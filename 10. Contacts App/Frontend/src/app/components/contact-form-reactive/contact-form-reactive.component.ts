import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ContactApiService } from 'src/app/services/contact-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-form-reactive',
  templateUrl: './contact-form-reactive.component.html',
  styleUrls: ['./contact-form-reactive.component.scss']
})
export class ContactFormReactiveComponent implements OnInit {

  contactForm: FormGroup;

  constructor(
    private contactService: ContactApiService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  get phone() { return this.contactForm.get('phone'); }
  get additionalPhones() { return this.contactForm.get('additionalPhones') as FormArray; }

  ngOnInit() {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      surname: ['', [Validators.maxLength(100)]],
      phone: ['', [Validators.pattern('[0-9,-]{2,100}'), Validators.required, Validators.maxLength(100)]],
      email: ['', [Validators.email ,Validators.maxLength(100)]],
      additionalPhones: this.fb.array([])
    });
  }

  onAddPhoneClick() {
    this.additionalPhones.push(this.fb.control(''));
  }

  onPhoneDeleteClick(index: number) {
    this.additionalPhones.removeAt(index);
  }

  async onSubmit() {
    let contact = await this.contactService.create(this.contactForm.value);
    this.router.navigate(['/contact', contact.id])
  }
}

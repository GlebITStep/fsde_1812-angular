import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { ContactApiService } from 'src/app/services/contact-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  model: Contact = {
    id: null,
    name: '',
    phone: ''
  };

  constructor(
    private contactService: ContactApiService,
    private router: Router) { }

  ngOnInit() {
  }

  async onSubmit() {
    let contact = await this.contactService.create(this.model);
    this.router.navigate(['/contact', contact.id])
  }

}

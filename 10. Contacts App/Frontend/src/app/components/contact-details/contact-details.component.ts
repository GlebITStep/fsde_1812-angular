import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';
import { ActivatedRoute } from '@angular/router';
import { Contact } from 'src/app/models/contact';
import { ContactApiService } from 'src/app/services/contact-api.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {

  contact: Contact;

  constructor(
    private contactService: ContactApiService,
    private route: ActivatedRoute) { }

  async ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.contact = await this.contactService.getById(id); 
  }

}

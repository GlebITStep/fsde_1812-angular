import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { ErrorComponent } from './components/error/error.component';
import { ContactService } from './services/contact.service';
import { ContactMemoryService } from './services/contact-memory.service';
import { LogPipe } from './pipes/log.pipe';
import { ContactFormReactiveComponent } from './components/contact-form-reactive/contact-form-reactive.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactFormComponent,
    ContactDetailsComponent,
    ErrorComponent,
    LogPipe,
    ContactFormReactiveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: ContactService, useClass: ContactMemoryService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

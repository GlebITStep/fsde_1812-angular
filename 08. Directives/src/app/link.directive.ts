import { Directive, ElementRef, OnInit, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appLink]'
})
export class LinkDirective implements OnInit {

  @Input('appLink') color: string;
  originalText: string;

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.originalText = this.element.nativeElement.innerText;
    this.color = this.color || 'red';
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.element.nativeElement.style = `background: ${this.color}`;
    let href = this.element.nativeElement.href;
    this.element.nativeElement.innerText = href;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.element.nativeElement.style = 'background: none';
    this.element.nativeElement.innerText = this.originalText;
  }

}

import { Injectable, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  private _currentLang: string = 'en';
  public changed: EventEmitter<string> = new EventEmitter<string>();

  constructor(private cookieService: CookieService) { 
    if (this.cookieService.check('lang')) 
      this._currentLang = this.cookieService.get('lang');
  }

  get currentLang() {
    return this._currentLang;
  }

  set currentLang(lang: string) {
    this.cookieService.set('lang', lang);
    this._currentLang = lang;
    this.changed.emit(lang);
  }
}

import { Directive, Input, ElementRef, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { LangService } from './lang.service';

@Directive({
  selector: '[appLang]'
})
export class LangDirective implements OnInit {

  @Input('appLang') lang: string;

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    private langService: LangService) { }

  ngOnInit(): void {
    this.render();
    this.langService.changed.subscribe(x => this.render());
  }

  private render() {
    this.container.clear();
    if (this.langService.currentLang == this.lang) {
      this.container.createEmbeddedView(this.template); 
    }
  }

}

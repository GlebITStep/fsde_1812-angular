import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { LangService } from './lang.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  json: string = null;
  show: boolean = true;

  constructor(private langService: LangService) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.json = 'Hello, from json!';
    }, 1000);
  }

  onLanguageChange(lang: string): void {
    this.langService.currentLang = lang;
  }
}

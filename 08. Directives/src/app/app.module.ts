import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LinkDirective } from './link.directive';

import { CookieService } from 'ngx-cookie-service';
import { LangDirective } from './lang.directive';

@NgModule({
  declarations: [
    AppComponent,
    LinkDirective,
    LangDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { Guid } from 'src/app/Helpers/guid';
import { TaskRepositoryService } from 'src/app/services/task-repository.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
    public taskRepository: TaskRepositoryService) {  
  }

  ngOnInit(): void {
    this.taskRepository.load();
  }

  addTask(taskName: string): void {
    this.taskRepository.add(taskName);
  }

  removeTask(removedTask: Task): void {
    this.taskRepository.remove(removedTask.id);
  }

}

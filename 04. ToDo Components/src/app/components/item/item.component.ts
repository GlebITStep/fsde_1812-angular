import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {

  @Input() item: Task = null;
  @Output() removed: EventEmitter<Task> = new EventEmitter<Task>();

  onRemoveClick(): void {
    console.log(this.item.id); 
    this.removed.emit(this.item);
  }

}

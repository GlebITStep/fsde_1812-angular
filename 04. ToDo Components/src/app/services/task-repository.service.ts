import { Injectable } from '@angular/core';
import { Task } from '../models/task';
import { Guid } from '../Helpers/guid';

@Injectable({
  providedIn: 'root'
})
export class TaskRepositoryService {
  
  private _tasks: Array<Task> = []; 

  get tasks(): ReadonlyArray<Task> {
    return this._tasks;
  }

  add(title: string) {
    let newTask = { 
      id: Guid.newGuid(), 
      title: title
    };
    this._tasks.push(newTask);
    this.save();
  }

  remove(id: string) {
    this._tasks = this._tasks.filter(x => x.id != id);
    this.save();
  }

  save(): void {
    localStorage.setItem('tasks', JSON.stringify(this._tasks));
  }

  load(): void {
    let json = localStorage.getItem('tasks');
    if (json) {
      this._tasks = JSON.parse(json);
    }
  }
}
